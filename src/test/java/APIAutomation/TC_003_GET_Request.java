package APIAutomation;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class TC_003_GET_Request {
	
	@Test
	void getsingleResource()
	{
		//specify baseURI
		RestAssured.baseURI = "https://reqres.in/api";
		// request object
		RequestSpecification httprequest = RestAssured.given();
		// response object 
		Response response = httprequest.request(Method.GET,"/unknown/2");
		
		//print response in console window		
		String responseBody = response.getBody().asString();
		System.out.println("Response body is : " + responseBody);
		
		//status code validation
		int statuscode = response.getStatusCode();
		System.out.println("status code is : " + statuscode);
		Assert.assertEquals(statuscode, 200);
		System.out.println("status line is : " +response.getStatusLine());
		
		// capture the header
		String contenttype = response.header("Content-Type");
		System.out.println("	Content type is : " + contenttype);
		Assert.assertEquals(contenttype, "application/json; charset=utf-8");
		
		String contentencoding = response.header("Content-Encoding");
		System.out.println("	Content encoding is : " + contentencoding);
	}

}
