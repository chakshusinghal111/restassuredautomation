package APIAutomation;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


@Test
public class TC_001_Get_Request {
	
	@Test
	void getListUser()
	{ 
		
		//specify baseURI
		RestAssured.baseURI = "https://reqres.in/api";
		// request object
		RequestSpecification httprequest = RestAssured.given();
		// response object 
		Response response = httprequest.request(Method.GET,"/users?page=2");
		
		//print response in console window		
		String responseBody = response.getBody().asString();
		System.out.println("Response body is : " + responseBody);
		
		//status code validation
		int statuscode = response.getStatusCode();
		System.out.println("status code is : " + statuscode);
		Assert.assertEquals(statuscode, 200);
		System.out.println("status line is : " +response.getStatusLine());
		
		ArrayList data = (ArrayList)  response.jsonPath().get("data");
		System.out.println(data);
		
//		for ( int i = 0 ; i < data.size(); i++)
//		{			
//			System.out.println(data.get(i));
//			
//		} 
		
		String name =  response.jsonPath().get("data[0].email");
		System.out.println("email id is : " + name);
		int id =  response.jsonPath().get("data[0].id");
		System.out.println("id is : " + id);
		
//		String city = response.jsonPath().get("address.city");
//		System.out.println(city);
//		
//		Assert.assertEquals(city,"delhi");
	}

}
