package APIAutomation;

import java.io.File;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.authentication.PreemptiveBasicAuthScheme;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class TC_002_POST_Request {
	
	@Test
	void createUser()
	{
		
		File jsonDataInFile = new File( System.getProperty("user.dir") + File.separator + "payload" + File.separator + "payload.json");
		
		RestAssured.baseURI = "https://reqres.in/api";
		// request object
		 
//		PreemptiveBasicAuthScheme basicauth = new PreemptiveBasicAuthScheme();
//		basicauth.setUserName("username");
//		basicauth.setPassword("paswd");
//		RestAssured.authentication = basicauth;
		
		RequestSpecification httprequest = RestAssured.given();	
		
		//request payload along with post request		
//		JSONObject requestParams = new JSONObject();		
//		requestParams.put("name", "Shaym kumar");
//		requestParams.put("job", "Tester");
// 		httprequest.body(requestParams.toJSONString());   // attach data to the request
		
		httprequest.header("Content-Type","application/json");
		httprequest.body(jsonDataInFile);
		
		Response response = httprequest.request(Method.POST,"/users");
		
		String responsebody = response.body().asString();
		System.out.println(responsebody);
		//status code validation
		int statuscode = response.getStatusCode();
		System.out.println("status code is : " + statuscode);
		Assert.assertEquals(statuscode, 201);		
				
		String namedata = response.jsonPath().get("name");
		System.out.println(namedata);
		Assert.assertEquals(namedata, "Shaym kumar");
	}

}
